(ns little-clojurer.numbers-games
  (:use [little-clojurer.cons-the-magnificent]
        [little-clojurer.toys]
        [little-clojurer.preface]))

(atom? 14)
; true

(number? -3)
; true

(number? 3.14159)
; true

(defn add1
  [n]
  (+ n 1))

(add1 67)
; 68

(defn sub1
  [n]
  (- n 1))

(sub1 5)
; 4

(sub1 0)
; different to scheme
; -1

(zero? 0)
; true

(zero? 1492)
; false

(+ 46 12)
; 58

(defn plus
  [x y]
  (if (zero? x)
    y
    (plus (sub1 x) (add1 y))))

(plus 46 12)
; 58

(- 14 3)
; 11

(- 17 9)
; 8

(defn minus
  [x y]
  (if (zero? y)
    x
    (minus (sub1 x) (sub1 y))))

(minus 14 3)
; 11

(minus 17 9)
; 8

(defn tup?
  [x] 
  (and (list? x)
       (every? number? x)))

(tup? '(2 11 3 79 47 6))
; true

(tup? '(8 55 5 555))
; true

(tup? '(1 2 8 apple 4 3))
; false

(tup? '(3 (7 4) 13 9))
; false

(tup? '())
; true

;(defn addtup
;  [x]
;  (apply + x))

(defn addtup
  [x]
  (if (null? x)
    0
    (+ (car x) (addtup (cdr x)))))

(addtup '(3 5 2 8))
; 18

(addtup '(15 6 7 12 3))
; 43

(* 5 3)
; 15

(* 13 4)
; 52

(defn times
  [x y]
  (if (zero? x)
    0
    (plus y (times (sub1 x) y))))

(times 5 3)
; 15


(times 12 3)
; 36

(defn tup+
  [tup1 tup2]
  (cond
    (null? tup1) tup2
    (null? tup2) tup1
    :else (cons (plus (car tup1) (car tup2))
                (tup+ (cdr tup1) (cdr tup2)))))

(tup+ '(3 6 9 11 4) '(8 5 2 0 7))
; (11 11 11 11 11)

(tup+ '(2 3) '(4 6))
; (6 9)

(tup+ '(3 7) '(4 6 8 1))
; (7 13 8 1)

(> 12 133)
; false

(> 120 11)
; true

(defn gt
  [x y]
  (cond
    (zero? x) false
    (zero? y) true
    :else (gt (sub1 x) (sub1 y))))

(gt 12 133)
; false

(gt 120 11)
; true

(gt 3 3)
; false

(< 4 6)
; true

(< 8 3)
; false

(< 6 6)
; false

(defn lt
  [x y]
  (cond
    (zero? y) false
    (zero? x) true
    :else (lt (sub1 x) (sub1 y))))

(lt 4 6)
; true

(lt 8 3)
; false

(lt 6 6)
; false

(defn eq
  [x y]
  (not (or (gt x y)
            (lt x y))))

(eq 1 1)
; true

(eq 1 0)
; false

(defn exp
  [x n]
  (if (zero? n)
    1
    (* x (exp x (sub1 n)))))

(exp 1 1)
; 1

(exp 2 3)
; 8

(exp 5 3)
; 125

(defn divide
  [x y]
  (if (< x y)
    0
    (inc (divide (- x y) y))))

(divide 15 4)
; 3

(defn length
  [l]
  (if (empty? l)
    0
    (inc (length (next l)))))

(length '(hotdogs with mustard sauerkraut and pickles))
; 6

(length '(ham and cheese on rye))
; 5

(defn pick 
  [n l]
  (if (= 1 n)
    (car l)
    (pick (sub1 n) (cdr l))))

(pick 4 '(lasagne spaghetti ravioli macaroni meatball))
; macaroni

(pick 0 '(a))
; error

(defn rempick
  [n l]
  (if (= 1 n)
    (cdr l)
    (cons (car l) (rempick (sub1 n) (cdr l)))))

(rempick 3 '(hotdogs with hot mustard))
; (hotdogs with mustard)

(number? 'tomato)
; false

(number? 75)
; true

(defn no-nums
  [l]
  (cond 
    (null? l) '()
    (number? (car l)) (recur (cdr l))
    :else (cons (car l) (no-nums (cdr l)))))

(no-nums '(5 pears 6 prunes 9 dates))
; (pears prunes dates)

(defn all-nums
  [l]
  (cond 
    (null? l) '()
    (number? (car l)) (cons (car l) (all-nums (cdr l)))
    :else (recur (cdr l))))

(all-nums '(5 pears 6 prunes 9 dates))
; (5 6 9)

(defn eqan?
  [a1 a2]
  (cond
    (and (number? a1) (number? a2)) true
    (or (number? a1) (number? a2)) false
    :else (eq a1 a2)))

(defn occur
  [a l]
  (cond
    (null? l) 0
    (= a (car l)) (inc (occur a (cdr l)))
    :else (recur a (cdr l))))

(defn one?
  [x]
  (= 1 x))

(one? 1)
; true

(one? 2)
; false

