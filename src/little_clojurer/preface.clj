(ns little-clojurer.preface)

(def atom?
  (complement coll?))

(atom (quote ()))
