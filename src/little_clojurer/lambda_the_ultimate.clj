(ns little-clojurer.lambda-the-ultimate
  (:use [little-clojurer.cons-the-magnificent]
        [little-clojurer.do-it-again]
        [little-clojurer.oh-my-gawd]
        [little-clojurer.preface]
        [little-clojurer.shadows]
        [little-clojurer.toys]
        [little-clojurer.friends-and-relations]))

(rember 'a '(b c a))
;; (b c)

(rember 'c '(b c a))
;; (b a)

(defn rember-f
  [test? a l]
  (cond
    (null? l) l
    (test? a (car l)) (cdr l)
    :else (cons (car l) (rember a (cdr l)))))

(rember-f = '5 '(6 2 5 3))
;; (6 2 3)

(defn =-c
  [a]
  (fn [x]
    (= x a)))

(=-c 'salad)
;; (fn [x] (= x 'salad))

(def =-salad (=-c 'salad))

(=-salad 'salad)
;; true

(=-salad 'tuna)
;; false

((=-c 'salad) 'tuna)
;; false

(defn rember-f
  [test?]
  (fn fun [a l]
    (cond
     (empty? l) '()
     (test? a (first l)) (next l)
     :else (cons (first l) (fun a (next l))))))

(def rember-= (rember-f =))

(rember-= 'tune '(tuna salad is good))
;; (tuna salad is good)

(rember-= 'tuna '(shrimp salad and tuna salad))
;; (shrimp salad and salad)

((rember-f =) '= '(equal? = eqan? eqlist? eqpair?))
;; (equal? eqan? eqlist? eqpair?)

(defn insertL-f
  [test?]
  (fn [new old l]
    (cond
     (empty? l) '()
     (test? (first l) old) (cons new l)
     :else (cons (first l) ((insertL-f test?) new old (next l))))))

(defn insertR-f
  [test?]
  (fn [new old l]
    (empty? l) '()
    (test? (first l) old) (cons old (cons new (next l)))
    :else (cons (first l) ((insertR-f test?) new old (next l)))))

(defn insert-g
  [seq-fn]
  (fn
    [new old l]
    (cond
     (empty? l) '()
     (= (first l) old) (seq-fn new old (next l))
     :else (cons (first l) ((insert-g seq-fn) new old (next l))))))

(defn seqL
  [new old l]
  (cons new (cons old l)))

(defn seqR
  [new old l]
  (cons old (cons new l)))

(def insertL' (insert-g seqL))
(def insertR' (insert-g seqR))

(def insertL'' (insert-g #(cons %1 (cons %2 %3))))
(def insertR'' (insert-g #(cons %2 (cons %1 %3))))

(insertL' 'a 'b '(1 a 2 b c))
;; (1 a 2 a b c)

(insertR' 'a 'b '(1 a 2 b c))
;; (1 a 2 b a c)

(insertL'' 'a 'b '(1 a 2 b c))
;; (1 a 2 a b c)

(insertR'' 'a 'b '(1 a 2 b c))
;; (1 a 2 b a c)

(defn seqS [new old l]
  (cons new l))

(def subst' (insert-g seqS))

(subst' 1 2 '(1 2 3))
;; (1 1 3)
