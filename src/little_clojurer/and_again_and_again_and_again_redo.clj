(ns little-clojurer.and-again-and-again-and-again
  (:use [little-clojurer.cons-the-magnificent]
        [little-clojurer.do-it-again]
        [little-clojurer.oh-my-gawd]
        [little-clojurer.preface]
        [little-clojurer.shadows]
        [little-clojurer.toys]
        [little-clojurer.friends-and-relations]
        [little-clojurer.lambda-the-ultimate]))

(defn pick
  [pos lat]
  (nth lat (dec pos)))

(defn keep-looking
  [target current lat]
  (if (number? current)
    (recur target (pick current lat) lat)
    (= target current)))

(defn looking
  [a lat]
  (keep-looking a (pick 1 lat) lat))


(looking 'caviar '(6 2 4 caviar 5 7 3))
;; true


(looking 'caviar '(6 2 grits caviar 5 7 3))
;; false

(defn eternity
  [x]
  (recur x))

(defn shift
  [lat]
  (list (first (first lat))
        (list (second (first lat))
              (second lat))))

(shift '((a b) c))
;; (a (b c))

(shift '((a b) (c d)))
;; (a (b (c d)))

(defn align
  [pora]
  (cond
    (atom? pora) pora
    (a-pair? (first pora)) (recur (shift pora))
    :else (list (first pora)
                (align (second pora)))))

(defn length*
  [pora]
  (cond
    (atom? pora) 1
    (list? pora) (let [x (first pora)
                       xs (second pora)]
                   (+ (length* x)
                      (length* xs)))))

(defn weight*
  [pora]
  (cond
    (atom? pora) 1
    :else (+ (* (weight* (first pora)) 2)
             (weight* (second pora)))))

(weight* '((a b) c))
;; 7

(weight* '(a (b c)))
;; 5

(defn shuffle
  [pora]
  (cond
    (atom? pora) pora
    (a-pair? (first pora)) (recur (revpair pora))
    :else (list (first pora)
                (shuffle (second pora)))))

(shuffle '(a (b c)))
;; (a (b c))

(shuffle '(a b))
;; (a b)

;; (shuffle '((a b) (c d)))
;; Never terminates

(defn C
  [n]
  (cond
    (= 1 n) 1
    :else (cond
            (even? n) (recur (/ n 2))
            :else (recur (inc (* 3 n))))))


(defn A
  [n m]
  (cond (= 0 n) (inc m)
        (= 0 m) (recur (dec n) 1)
        :else (recur (dec n)
                     (A n (dec m)))))

(A 1 0)
;; 2

(A 1 1)
;; 3

(A 2 2)
;; 7

(def length0
  (fn
    [l]
    (cond (null? l) 0
          :else (eternity (rest l)))))

(def length1
  (fn
    [l]
    (cond (null? l) 0
          :else (inc
                 ((fn [l]
                    (cond (null? l) 0
                          :else (eternity (rest l))))
                  (rest l))))))

(length1 '())
;; 0

(length1 '(1))
;; 1

(def length2
  (fn
    [l]
    (cond (null? l) 0
          :else (inc
                 ((fn [l]
                    (cond (null? l) 0
                          :else (inc
                                 ((fn [l]
                                    (cond (null? l) 0
                                          :else (eternity (rest l))))
                                  (rest l)))))
                  (rest l))))))

(length2 '(1 2))
;; 2


(def length0
  ((fn [length]
     (fn [l]
       (cond (null? l) 0
             :else (inc (length (rest l))))))
   eternity))

(length0 '())
;; 0

;; (((fn [length]
;; (fn [l]
;;  (cond (null? l) 0
;;        :else (inc (length (rest l))))))
;; eternity) '())

;; ((fn [l]
;;  (cond (null? l) 0
;;        :else (inc (eternity (rest l))))) '())

;; (cond (null? '()) 0
;;       :else (inc (eternity (rest '()))))

;; 0

(def length1
  ((fn [length]
     (fn [l]
       (cond (null? l) 0
             :else (inc (length (rest l))))))
   ((fn [g]
      (fn [l]
        (cond (null? l) 0
              :else (inc (g (rest l))))))
    eternity)))

(length1 '())
;; 0

(length1 '(1))
;; 1

(def length2
  ((fn [length]
     (fn [l]
       (cond (null? l) 0
             :else (inc (length (rest l))))))
   ((fn [g]
      (fn [l]
        (cond (null? l) 0
              :else (inc (g (rest l))))))
    ((fn [h]
       (fn [l]
         (cond (null? l) 0
               :else (inc (h (rest l))))))
     eternity))))

(length2 '(1 2))
;; 2

(def length0
  ((fn [mk-length]
     (mk-length eternity))
   (fn [length]
     (fn [l]
       (cond (null? l) 0
             :else (inc (length (rest l))))))))

(length0 '())
;; 0

(def length1
  ((fn [mk-length]
     (mk-length
      (mk-length eternity)))
   (fn [length]
     (fn [l]
       (cond (null? l) 0
             :else (inc (length (rest l))))))))

(length1 '())
;; 0

(length1 '(1))
;; 1

(def length3
  ((fn [mk-length]
     (mk-length
      (mk-length
       (mk-length
        (mk-length eternity)))))
   (fn [length]
     (fn [l]
       (cond (null? l) 0
             :else (inc (length (rest l))))))))

(length3 '(1 2 3))
;; 3

(def length0
  ((fn [mk-length]
     (mk-length mk-length))
   (fn [length]
     (fn [l]
       (cond (null? l) 0
             :else (inc (length (rest l))))))))

(length0 '())
;; 0

(def length0
  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     (fn [l]
       (cond (null? l) 0
             :else (inc (mk-length (rest l))))))))

(def length-x
  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     (fn [l]
       (cond (null? l) 0
             :else (inc ((mk-length mk-length) (rest l))))))))

(length-x '(1 2 3 4 5))
;; 5

(((fn [mk-length]
    (mk-length mk-length))
  (fn [mk-length]
    (fn [l]
      (cond (null? l) 0
            :else (inc ((mk-length eternity) (rest l)))))))
 '(apples))

(((fn [mk-length]
    (fn [l]
      (cond (null? l) 0
            :else (inc ((mk-length eternity) (rest l))))))
  (fn [mk-length]
    (fn [l]
      (cond (null? l) 0
            :else (inc ((mk-length eternity) (rest l)))))))
 '(apples))

((fn [l]
   (cond (null? l) 0
         :else (inc (((fn [mk-length]
                        (fn [l]
                          (cond (null? l) 0
                                :else (inc ((mk-length eternity)
                                            (rest l))))))
                      eternity)
                     (rest l)))))
 '(apples))

((fn [l]
   (cond (null? l) 0
         :else (inc ((fn [l]
                       (cond (null? l) 0
                             :else (inc ((eternity eternity)
                                         (rest l)))))
                     (rest l)))))
 '(apples))

((fn [l]
   (cond (null? l) 0
         :else (inc
                (cond (null? (rest l)) 0
                      :else (inc ((eternity eternity)
                                  (rest (rest l))))))))
 '(apples))

(cond (null? '(apples)) 0
      :else (inc
             (cond (null? (rest '(apples))) 0
                   :else (inc ((eternity eternity)
                               (rest (rest '(apples))))))))

(inc
 (cond (null? (rest '(apples))) 0
       :else (inc ((eternity eternity)
                   (rest (rest '(apples)))))))

(inc
 (cond (null? '()) 0
       :else (inc ((eternity eternity)
                   (rest (rest '(apples)))))))

(inc 0)

1

(def length
  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     (fn [l]
       (cond (null? l) 0
             :else (inc ((mk-length mk-length) (rest l))))))))

(length '(1 2 3))
;; 3

;;(def bad-length
;;  ((fn [mk-length]
;;     (mk-length mk-length))
;;   (fn [mk-length]
;;     ((fn [length]
;;        (fn [l]
;;          (cond (null? l) 0
;;                :else (inc (length (rest l))))))
;;      (mk-length mk-length)))))

(def length
  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     ((fn [length]
        (fn [l]
          (cond (null? l) 0
                :else (inc (length (rest l))))))
      (fn [l]
        ((mk-length mk-length) l))))))

(length '(1 2 3))
;; 3

(def length
  ((fn [le]
     ((fn [mk-length]
        (mk-length mk-length))
      (fn [mk-length]
        (le (fn [l]
              ((mk-length mk-length) l))))))
   (fn [length]
     (fn [l]
       (cond (null? l) 0
             :else (inc (length (rest l))))))))

(length '(1 2 3 4))
;; 4

(def le-first
  (fn [le]
    ((fn [mk-length]
       (mk-length mk-length))
     (fn [mk-length]
       (le (fn [l]
             ((mk-length mk-length) l)))))))

(def le-second
  (fn [length]
    (fn [l]
      (cond (null? l) 0
            :else (inc (length (rest l)))))))

((le-first le-second) '())
;; 0

((le-first le-second) '(1 2 3 4))
;; 4

(def Y
  (fn [le]
    ((fn [f] (f f))
     (fn [f]
       (le (fn [x] ((f f) x)))))))

(def length
  (Y (fn [length]
       (fn [l]
         (cond (null? l) 0
               :else (inc (length (rest l))))))))

(length '(1 2 3 4 5))
;; 5
