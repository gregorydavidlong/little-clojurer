(ns little-clojurer.shadows
  (:use [little-clojurer.cons-the-magnificent]
        [little-clojurer.do-it-again]
        [little-clojurer.oh-my-gawd]
        [little-clojurer.preface]
        [little-clojurer.toys]))

(let [y 'a]
  (= y 'a))
;; true

(defn numbered?
  [x]
  (cond
   (number? x)
   true

   (symbol? x)
   false

   (or (= (second x) '*)
       (= (second x) '+)
       (= (second x) 'exp))
   (and (numbered? (first x))
        (numbered? (second (next x))))

   :default false))


(numbered? 1)
;; true

(numbered? '1)
;; true

(numbered? 'sausage)
;; false

(defn exp
  [x y]
  (if (= 0 y)
    1
    (* x (exp x (- y 1)))))

(exp 2 4)
;; 16

(numbered? '(3 + (4 exp 5)))
;; true

(numbered? '(2 * sausage))
;; false

(numbered? '(2 + 3))
;; true

(defn value2
  [x]
  (cond

   (atom? x)
   x

   (list? x)
   (cond
    (= '+ (second x))
    (+
     (value2 (first x))
     (value2 (second (next x))))

    (= '* (second x))
    (*
     (value2 (first x))
     (value2 (second (next x))))

    (= 'exp (second x))
    (exp
     (value2 (first x))
     (value2 (second (next x)))))

   :default
   x))

(value2 1)
;; 1

(value2 '1)
;; 1

(value2 '(1 + 3))
;; 4

(value2 '(1 + (1 + 1)))
;; 3

(value2 '(1 + (3 exp 4)))
;; 82

(value2 'cookie)
;; cookie

(def operator first)

(def _1st-sub-exp second)

(defn _2nd-sub-exp [x] (second (next x)))

(defn value3
  [x]
  (cond

   (atom? x)
   x

   (list? x)
   (cond
    (= '+ (operator x))
    (+
     (value3 (_1st-sub-exp x))
     (value3 (_2nd-sub-exp x)))

    (= '* (operator x))
    (*
     (value3 (_1st-sub-exp x))
     (value3 (_2nd-sub-exp x)))

    (= 'exp (operator x))
    (exp
     (value3 (_1st-sub-exp x))
     (value3 (_2nd-sub-exp x))))

   :default
   x))

(value3 '(+ 1 3))
;; 4

(= (first '(+ 1 3)) '+)
;; true

(defn sero?
  [x]
  (= '() x))

(sero? '())
;; true

(defn edd1
  [x]
  (if (sero? x)
    '(())
    (cons '() x)))

(edd1 '())
;; '(())

(edd1 '(()))
;; (() ())

(defn sub1
  [x]
  (if (= '(()) x)
    '()
    (next x)))

(sub1 '(() ()))
;; '(())

(sub1 '(()))
;; '()

(defn plus-
  [x y]
  (if (sero? y)
    x
    (recur (edd1 x) (sub1 y))))

(plus- '() '(()))
;; (())

(plus- '(()) '(()))
;; (() ())
