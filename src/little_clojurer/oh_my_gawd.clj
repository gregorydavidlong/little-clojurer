(ns little-clojurer.oh-my-gawd
  (:use [little-clojurer.toys]
        [little-clojurer.cons-the-magnificent]
        [little-clojurer.do-it-again]
        [little-clojurer.preface]))

(defn rember*
  [a l]
  (cond
   (empty? l) l
   (= (first l) a) (rember* a (next l))
   (list? (first l)) (cons (rember* a (first l)) (rember* a (next l)))
   :else (cons (first l) (rember* a (next l)))))

(rember* 'cup '((coffee) cup ((tea) cup) (and (hick)) cup))
; ((coffee) ((tea)) (and (hick)))

(rember* 'sauce '(((tomato sauce)) ((bean) sauce) (and ((flying)) sauce)))
;(((tomato)) ((bean)) (and ((flying))))

(lat? '(((tomato sauce)) ((bean sauce) (and ((flying)) sauce))))
;false

(atom? (car '(((tomato sauce)) ((bean sauce) (and ((flying)) sauce)))))
;false

(defn insertR*
  [new old l]
  (cond
   (empty? l) l
   (= old (first l)) (cons old (cons new (insertR* new old (next l))))
   (list? (first l)) (cons (insertR* new old (first l)) (insertR* new old (next l)))
   :default (cons (first l) (insertR* new old (next l)))))

(let [new 'roast
      old 'chuck
      l '((how much (wood))
          could
          ((a (wood) chuck))
          (((chuck)))
          (if (a) ((wood chuck)))
          could chuck wood)]
  (insertR* new old l))
; ((how much (wood)) could ((a (wood) chuck roast)) (((chuck roast))) (if (a) ((wood chuck roast))) could chuck roast wood)

(defn occur*
  [a l]
  (cond
   (empty? l) 0
   (list? (first l)) (+ (occur* a (first l)) (occur* a (next l)))
   (= a (first l)) (inc (occur* a (next l)))
   :default (recur a (next l))))

(def occursomething occur*)

(let [a 'banana
      l '((banana)
          (split ((((banana ice)))
                  (cream (banana))
                  sherbert))
          (banana)
          (bread)
          (banana brandy))]
  (occursomething a l))
;5

(defn subst*
  [new old l]
  (cond
   (empty? l) l
   (list? (first l)) (cons (subst* new old (first l))
                           (subst* new old (next l)))
   (= old (first l)) (cons new
                           (subst* new old (next l)))
   :default (cons (first l) (subst* new old (next l)))))

(let [new 'orange
      old 'banana
      l '((banana)
          (split ((((banana ice)))
                  (cream (banana))
                  sherbert))
          (banana)
          (bread)
          (banana brandy))]
  (subst* new old l))
;((orange) (split ((((orange ice))) (cream (orange)) sherbert)) (orange) (bread) (orange brandy))

(defn insertL*
  [new old l]
  (cond
   (empty? l) l
   (list? (first l)) (cons (insertL* new old (first l))
                           (insertL* new old (next l)))
   (= old (first l)) (cons new
                           (cons (first l)
                                 (insertL* new old (next l))))
   :default (cons (first l)
                  (insertL* new old (next l)))))

(let [new 'pecker
      old 'chuck
      l '((how much (wood))
          could
          ((a (wood) chuck))
          (((chuck)))
          (if (a) ((wood chuck)))
          could chuck wood)]
  (insertL* new old l))
;; ((how much (wood)) could ((a (wood) pecker chuck)) (((pecker chuck))) (if (a) ((wood pecker chuck))) could pecker chuck wood)



(defn member*
  [a l]
  (cond
   (empty? l) false
   (= a (first l))  true
   (list? (first l)) (or (member* a (first l))
                         (member* a (next l)))
   :default (member* a (next l))))

(let [a 'chips
      l '((potato) (chips ((with) fish) (chips)))]
  (member* a l))
;; true

(member* 'chips '((potato) chips ((with) fish) (chips)))
;; true

(defn leftmost
  [l]
  (cond
   (atom? (first l)) (first l)
   (list? (first l)) (leftmost (first l))))

(leftmost '((potato) (chips ((with) fish) (chips))))
;; potato

(leftmost '(((hot) (tuna (and))) cheese))
;; host

(leftmost '(((() four)) 17 (seventeen)))
;; nil

(leftmost (quote ()))
;; nil

(let [x 'pizza
      l '(mozzarella pizza)]
  (and (atom? (first l))
       (= (first l) x)))
;; false

(let [x 'pizza
      l '((mozzarella mushroom) pizza)]
  (and (atom? (first l))
       (= (first l) x)))
;; false

(let [x 'pizza
      l '(pizza)]
  (and (atom? (first l))
       (= (first l) x)))
;; true

(defn eqlist?
  [l1 l2]
  (cond
   (and (empty? l1)
        (empty? l2))
   true

   (and (atom? (first l1))
        (atom? (first l2)))
   (and (= (first l1)
           (first l2))
        (eqlist? (next l1)
                 (next l2)))

   (and (list? (first l1))
        (list? (first l2)))
   (and (eqlist? (first l1)
                 (first l2))
        (eqlist? (next l1)
                 (next l2)))

   :default
   false))

(eqlist? '(strawberry ice cream)
         '(strawberry ice cream))
;; true

(eqlist? '(strawberry ice cream)
         '(strawberry cream ice))
;; false

(eqlist? '(banana ((split)))
         '((banana) (split)))
;; false?

(eqlist? '(beef ((sausage)) (and (soda)))
         '(beef ((salami)) (and (soda))))
;; false

(eqlist? '(beef ((sausage)) (and (soda)))
         '(beef ((sausage)) (and (soda))))
;; true
