(ns little-clojurer.what-is-the-value-of-all-of-this
  (:use [little-clojurer.cons-the-magnificent]
        [little-clojurer.do-it-again]
        [little-clojurer.oh-my-gawd]
        [little-clojurer.preface]
        [little-clojurer.shadows]
        [little-clojurer.toys]
        [little-clojurer.friends-and-relations]
        [little-clojurer.lambda-the-ultimate]
        [little-clojurer.and-again-and-again-and-again]))

;; Entries
'((a b c)
  (1 2 3))
'((appetizer entree beverage)
  (pate boeuf vin))

'((appetizer entree beverage)
  (beer beer beer))

'((beverage dessert)
  ((food is) (number one with us)))

(def new-entry list)

(defn lookup-in-entry
  [name entry]
  (let [keys (first entry)
        vals (second entry)]
    (cond
      (empty? keys) nil
      (= name (first keys)) (first vals)
      :else (recur name
                   (list (rest keys)
                         (rest vals))))))

(lookup-in-entry 'entree '((appetizer entree beverage)
                           (food tastes good)))
;; tastes

(defn lookup-in-entry-help
  [name names values entry-f]
  (cond (empty? names) (entry-f name)
        (= name (first names)) (first values)
        :else (recur name
                     (rest names)
                     (rest values)
                     entry-f)))

(defn lookup-in-entry
  [name entry entry-f]
  (lookup-in-entry-help name
                        (first entry)
                        (second entry)
                        entry-f))

(lookup-in-entry 'entree
                 '((appetizer entree beverage)
                   (food tastes good))
                 identity)
;; tastes

(def extend-table cons)

(defn lookup-in-table
  [name table table-f]
  (cond (empty? table) nil
        :else (lookup-in-entry name
                               (first table)
                               (fn [name]
                                 (lookup-in-table name
                                                  (rest table)
                                                  table-f)))))

(lookup-in-table 'entree
                 '(((entree dessert)
                    (spaghetti spumoni))
                   ((appetizer entree beverage)
                    (food tastes good)))
                 identity)
;; spaghetti

(lookup-in-table 'dessert
                 '(((entree dessert)
                    (spaghetti spumoni))
                   ((appetizer entree beverage)
                    (food tastes good)))
                 identity)
;; spumoni

(lookup-in-table 'appetizer
                 '(((entree dessert)
                    (spaghetti spumoni))
                   ((appetizer entree beverage)
                    (food tastes good)))
                 identity)
;; food

(rest '((a b c)))
;;()

(cons 'a (cons 'b (cons 'c '())))
;; (a b c)

(cons 'first
      (cons (cons 'quote
                  (cons (cons 'a
                              (cons 'b
                                    (cons 'c
                                          (quote ()))))
                        (quote ())))
            (quote ())))
;; (first (quote (a b c)))

(first (quote (a b c)))
;; a

(defn value
  [x]
  x)

(value (first (quote (a b c))))
;; a

(value (quote (first (quote (a b c)))))
;; (first (quote (a b c)))

(value (inc 6))
;; 7

(value 6)
;; 6

(value (quote nothing))
;; nothing

;; (value nothing)
;; ??

(value ((fn [nothing]
          (cons nothing (quote ())))
        (quote (from nothing comes something))))
;; ((from nothing comes something))

(value ((fn [nothing]
          (cond nothing (quote something)
                :else (quote nothing)))
        true))
;; something

(type 6)
;; java.lang.Long
;; *const

(type false)
;; java.lang.Boolean
;; *const

(value false)
;; false

(type cons)
;; clojure.core$cons
;; *const

(value first)
;; #<core$first clojure.core$first@58904bde>

(type (quote nothing))
;; clojure.lang.Symbol
;; *quote

;; (type nothing)
;; can't resolve nothing
;; *identifier

(type (fn [x y] (cons x y)))
;; little_clojurer.what_is_the_value_of_all_of_this$eval7644$fn__7645
;; *lambda

(type ((fn [nothing]
         (cond nothing (quote something)
               :else (quote nothing)))
       true))
;; clojure.lang.Symbol
;; *application

;;(type (cond nothing (quote something)
;;            :else (quote nothing)))
;; can't resolve nothing
;; *cond

;; *const
;; *quote
;; *identifier
;; *lambda
;; *cond
;; *application

(declare *const)
(declare *identifier)
(declare *quote)
(declare *lambda)
(declare *cond)
(declare *application)

(defn atom-to-action
  [e]
  (cond (number? e) *const
        (= e true) *const
        (= e false) *const
        (= e (quote cons)) *const
        (= e (quote car)) *const
        (= e (quote first)) *const
        (= e (quote cdr)) *const
        (= e (quote rest)) *const
        (= e (quote null?)) *const
        (= e (quote eq?)) *const
        (= e (quote =)) *const
        (= e (quote atom?)) *const
        (= e (quote zero?)) *const
        (= e (quote add1)) *const
        (= e (quote inc)) *const
        (= e (quote sub1)) *const
        (= e (quote dec)) *const
        (= e (quote number?)) *const
        :else *identifier))

(defn list-to-action
  [e]
  (cond (atom? (car e))
        (cond (= (car e) (quote quote))
              *quote
              (= (car e) (quote lambda))
              *lambda
              (= (car e) (quote fn))
              *lambda
              (= (car e) (quote cond))
              *cond
              :else *application)
        :else *application))

(defn expression-to-action
  [e]
  (cond (atom? e) (atom-to-action e)
        :else (list-to-action e)))

(defn meaning
  [e table]
  ((expression-to-action e) e table))

(defn value
  [e]
  (meaning e (quote ())))

(defn *const
  [e table]
  (cond (number? e) e
        (= e true) true
        (= e false) false
        :else (list (quote primitive) e)))

(*const 0 '())
;; 0

(*const true '())
;; true

(*const 'blah '())
;; (primitive blah)

(def text-of second) ;;?? str

(defn *quote
  [e table]
  (text-of e))

(defn initial-table
  [name]
  (first (quote ())))

(defn *identifier
  [e table]
  (lookup-in-table e table initial-table))

;; (value '(fn [x] x))
;; ?

(defn *lambda
  [e table]
  (list (quote non-primitive)
        (cons table (rest e))))

(meaning '(lambda (x) (cons x y))
         '(((y z) ((8) 9))))
;; (non-primitive ((((y z) ((8) 9))) [x] (cons x y)))

(def table-of first)
(def formals-of second)
(def body-of third)

(defn else?
  [x]
  (= 'else x))

(defn question-of
  [x]
  (first x))

(defn answer-of
  [x]
  (second x))

(defn evcon
  [lines table]
  (cond (else? (question-of (first lines)))
        (meaning (answer-of (first lines))
                 table)
        (meaning (question-of (first lines))
                 table)
        (meaning (answer-of (first lines))
                 table)
        :else (recur (rest lines) table)))

(def cond-lines-of rest)

(defn *cond
  [e table]
  (evcon (cond-lines-of e) table))

(*cond '(cond (coffee 1)
              (else 2))
       '(((coffee) (true))
         ((klatsch party) (5 (6)))))
;; 1

(*cond '(cond (coffee klatsch)
              (else party))
       '(((coffee) (true))
         ((klatsch party) (5 (6)))))
;; 5

(defn evlis
  [args table]
  (cond (empty? args) '()
        :else (cons (meaning (first args) table)
                    (evlis (rest args) table))))

(def function-of first)
(def arguments-of rest)

(declare apply)

(defn *application
  [e table]
  (apply (meaning (function-of e) table)
         (evlis (arguments-of e) table)))

(defn primitive?
  [e]
  (= (first e)
     'primitive))

(defn non-primitive?
  [e]
  (= (first e) 'non-primitive))

(declare apply-primitive)
(declare apply-closure)

(defn apply
  [fun vals]
  (cond (primitive? fun)
        (apply-primitive (second fun)
                         vals)
        (non-primitive? fun)
        (apply-closure (second fun)
                       vals)
        :else :apply-undefined))

(defn xatom?
  [x]
  (cond (atom? x) true
        (null? x) false
        (= (first x) 'primitive) true
        (= (first x) 'non-primitive) true
        :else false))

(defn apply-primitive
  [name vals]
  (cond (= name 'cons)
        (cons (first vals) (second vals))

        (= name 'car)
        (first (first vals))

        (= name 'cdr)
        (rest (first vals))

        (= name 'null?)
        (null? (first vals))

        (= name 'eq?)
        (= (first vals) (second vals))

        (= name 'atom?)
        (xatom? (first vals))

        (= name 'zero?)
        (zero? (first vals))

        (= name 'add1)
        (inc (first vals))

        (= name 'sub1)
        (dec (first vals))

        (= name 'number?)
        (number? (first vals))

        :else :undefined))

(defn apply-closure
  [closure vals]
  (meaning (body-of closure)
           (extend-table (new-entry (formals-of closure)
                                    vals)
                         (table-of closure))))

(apply-closure '((((u v w)
                   (1 2 3))
                  ((x y z)
                   (4 5 6)))
                 (x y)
                 (cons z x))
               '((a b c)
                 (d e f)))
;; (6 a b c)

(meaning '(cons z x) '(((x y)
                        ((a b c) (d e f)))
                       (( u v w)
                        (1 2 3))
                       ((x y z)
                        (4 5 6))))
;; (6 a b c)
