(ns little-clojurer.and-again-and-again-and-again
  (:use [little-clojurer.cons-the-magnificent]
        [little-clojurer.do-it-again]
        [little-clojurer.oh-my-gawd]
        [little-clojurer.preface]
        [little-clojurer.shadows]
        [little-clojurer.toys]
        [little-clojurer.friends-and-relations]
        [little-clojurer.lambda-the-ultimate]))

;;(defn looking
;;  [val ls]
;;  (letfn [(l-alt [pos lss]
;;            (println pos lss)
;;            (if (= pos 1)
;;              (let [current (first lss)]
;;                (if (number? current)
;;                  (recur current ls)
;;                  (= val current)))
;;              (recur (dec pos) (next lss))))]
;;    (l-alt 1 ls)))

(defn pick
  [pos ls]
  (if (= pos 1)
    (first ls)
    (recur (dec pos) (next ls))))

(defn keep-looking
  [val current ls]
  (if (number? current)
    (recur val (pick current ls) ls)
    (= val current)))

(defn looking
  [val ls]
  (keep-looking val (pick 1 ls) ls))

(looking 'caviar '(6 2 4 caviar 5 7 3))
;; true

(looking 'caviar '(6 2 grits caviar 5 7 3))
;; false

(pick 6 '(6 2 4 caviar 5 7 3))
;; 7

;; (looking 'caviar '(7 1 2 caviar 5 6 3))
;; inifinite loop

(defn shift
  [ls]
  (let [fst (first ls)
        snd (second ls)]
    (list (first fst)
          (list (second fst)
                snd))))

(shift '((a b) c))
;; (a (b c))

(shift '((a b) (c d)))
;; (a (b (c d)))

(defn align
  [pora]
  (cond
    (atom? pora) pora
    (a-pair? (first pora)) (recur (shift pora))
    :else (list (first pora)
                (align (second pora)))))

(align '((1 2) (3 4)))
;; (1 (2 (3 4)))

(defn length*
  [pora]
  (cond
    (atom? pora) 1
    (list? pora) (+ (length* (first pora))
                    (length* (next pora)))))

(defn A
  [n m]
  (cond
    (zero? n) (inc m)
    (zero? m) (recur (dec n) 1)
    :else (recur (dec n)
                 (A n (dec m)))))

(A 1 0)
;; 2

(A 1 1)
;; 3

(A 2 2)
;; 7

(defn eternity
  [x]
  (recur x))

(def lambda-length-0
  ((fn [length]
     (fn [l]
       (cond (empty? l) 0
             :else (inc (length (next l))))))
   eternity))

(lambda-length-0 '())
;; 0

(def lambda-length-1
  ((fn [f]
     (fn [l]
       (cond (empty? l) 0
             :else (inc (f (next l))))))
   ((fn [g]
      (fn [l]
        (cond (empty? l) 0
              :else (inc (g (next l))))))
    eternity)))

(lambda-length-1 '())
;; 0

(lambda-length-1 '(1))
;; 1

(def lambda-length-2
  ((fn [f]
     (fn [l]
       (cond (empty? l) 0
             :else (inc (f (next l))))))
   ((fn [g]
      (fn [l]
        (cond (empty? l) 0
              :else (inc (g (next l))))))
    ((fn [h]
       (fn [l]
         (cond (empty? l) 0
               :else (inc (h (next l))))))
     eternity))))

(lambda-length-2 '())
;; 0

(lambda-length-2 '(1))
;; 1

(lambda-length-2 '(1 2))
;; 2

(def mk-length-0
  ((fn [mk-length]
     (mk-length eternity))
   (fn [f]
     (fn [l]
       (cond (empty? l) 0
             :else (inc (f (next l))))))))

(mk-length-0 '())
;; 0

(def mk-length-1
  ((fn [mk-length]
     (mk-length
      (mk-length eternity)))
   (fn [f]
     (fn [l]
       (cond (empty? l) 0
             :else (inc (f (next l))))))))

(mk-length-1 '())
;; 0

(mk-length-1 '(1))
;; 1

(def mk-length-2
  ((fn [mk-length]
     (mk-length
      (mk-length
       (mk-length eternity))))
   (fn [f]
     (fn [l]
       (cond (empty? l) 0
             :else (inc (f (next l))))))))

(mk-length-2 '(1 2))
;; 2

(def mk-length'
  ((fn [mk-length]
     (mk-length mk-length))
   (fn [length]
     (fn [l]
       (cond (empty? l) 0
             :else (inc (length (next l))))))))

(mk-length' '())
;; 0

;; Show execution with '()

(((fn [mk-length]
    (mk-length mk-length))
  (fn [length]
    (fn [l]
      (cond (empty? l) 0
            :else (inc (length (next l)))))))
 '())

((fn [l]
   (cond (empty? l) 0
         :else (inc ((fn [length]
                       (fn [l]
                         (cond (empty? l) 0
                               :else (inc (length (next l)))))) (next l)))))
 '())

(cond (empty? '()) 0
      :else (inc ((fn [length]
                    (fn [l]
                      (cond (empty? l) 0
                            :else (inc (length (next l)))))) (next '()))))

;; Execution with '(1)
;; ...
;;(cond (empty? '(1)) 0
;;      :else (inc ((fn [length]
;;                    (fn [l]
;;                      (cond (empty? l) 0
;;                            :else (inc (length (next l)))))) (next '(1)))))
;;
;;(inc ((fn [length]
;;        (fn [l]
;;          (cond (empty? l) 0
;;                :else (inc (length (next l)))))) (next '(1))))
;;
;;(inc ((fn [length]
;;        (fn [l]
;;          (cond (empty? l) 0
;;                :else (inc (length (next l)))))) '()))
;;
;;(inc (fn [l]
;;       (cond (empty? l) 0
;;             :else (inc ('() (next l))))))


;;(mk-length* '(1 2))

(def mk-length''
  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     (fn [l]
       (cond (empty? l) 0
             :else (inc (mk-length (next l))))))))

(((fn [mk-length]
    (mk-length mk-length))
  (fn [mk-length]
    (fn [l]
      (cond (empty? l) 0
            :else (inc ((mk-length eternity)
                        (next l)))))))
 '(apples))
;; 1

;; Execution
(((fn [mk-length]
    (fn [l]
      (cond (empty? l) 0
            :else (inc ((mk-length eternity)
                        (next l))))))
  (fn [mk-length]
    (fn [l]
      (cond (empty? l) 0
            :else (inc ((mk-length eternity)
                        (next l)))))))
 '(apples))

(cond (empty? '(apples)) 0
      :else (inc (((fn [mk-length]
                     (fn [l]
                       (cond (empty? l) 0
                             :else (inc ((mk-length eternity)
                                         (next l)))))) eternity)
                  (next '(apples)))))

(inc (((fn [mk-length]
         (fn [l]
           (cond (empty? l) 0
                 :else (inc ((mk-length eternity)
                             (next l)))))) eternity)
      (next '(apples))))

(inc (((fn [mk-length]
         (fn [l]
           (cond (empty? l) 0
                 :else (inc ((mk-length eternity)
                             (next l)))))) eternity)
      '()))

(inc ((fn [l]
        (cond (empty? l) 0
              :else (inc ((eternity eternity)
                          (next l)))))
      '()))

(inc (cond (empty? '()) 0
           :else (inc ((eternity eternity)
                       (next '())))))

(inc 0)

1

(((fn [mk-length]
    (mk-length mk-length))
  (fn [mk-length]
    (fn [l]
      (cond (empty? l) 0
            :else (inc
                   ((mk-length mk-length)
                    (next l)))))))
 '(1 2 3))
;; 3


;; Stack overflow
;;(((fn [mk-length]
;;    (mk-length mk-length))
;;  (fn [mk-length]
;;    ((fn [length]
;;       (fn [l]
;;         (cond (empty? l) 0
;;               :else (inc
;;                      (length (next l))))))
;;     (mk-length mk-length))))
;; '(apples))

(((fn [mk-length]
    (mk-length mk-length))
  (fn [mk-length]
    (fn [l]
      (cond (empty? l) 0
            :else (inc
                   ((fn [x]
                      ((mk-length mk-length) x))
                    (next l)))))))
 '(1 2 3))
;; 3

(((fn [mk-length]
    (mk-length mk-length))
  (fn [mk-length]
    ((fn [length]
       (fn [l]
         (cond (empty? l) 0
               :else (inc
                      (length
                       (next l))))))
     (fn [x]
       ((mk-length mk-length) x)))))
 '(1 2 3))
;; 3

(((fn [le]
    ((fn [mk-length]
       (mk-length mk-length))
     (fn [mk-length]
       (le
        (fn [x]
          ((mk-length mk-length) x))))))
  (fn [length]
    (fn [l]
      (cond (empty? l) 0
            :else (inc (length (next l)))))))
 '(1 2 3))

(fn [le]
  ((fn [mk-length]
     (mk-length mk-length))
   (fn [mk-length]
     (le (fn [x]
           ((mk-length mk-length) x))))))

(defn Y
  [le]
  ((fn [f] (f f))
   (fn [f] (le (fn [x] ((f f) x))))))
