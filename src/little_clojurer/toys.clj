(ns little-clojurer.toys
  (:use [little-clojurer.preface]))

(atom? atom)

(atom? "turkey")

(atom? 1492)

(atom? \u)

(atom? "*abc$")

(list? '(atom))

(list? '(atom turkey or))

#_(list? '(atom turkey) or)

(list? '((atom turkey) or))

(defn s-expr?
  [x]
  (or (atom? x) (list? x)))

(s-expr? "xyz")

(s-expr? '(x y z))

(s-expr? '((x y) z))

(list? '(how are you doing so far))

'(how are you doing so far) ; 6 s-expressions

(list? '(((how) are) ((you) (doing so)) far))

'(((how) are) ((you) (doing so)) far) ; 3 s-expressions

(list? '())

(atom? '())

(list? '(() () () ()))

(defn car
  [l]
  (first l))

(car '(a b c))

(car '((a b c) x y z))

(car "hotdog") ; Differs from book

(car '())

(car '(((hotdogs)) (and) (pickle) relish))

(car (car '(((hotdogs)) (and))))

(defn cdr
  [l]
  (next l))

(cdr '(a b c))

(cdr '((a b c) x y z))

(cdr '(hamburger)) ; Differs from book

(cdr '((x) t r))

#_(cdr 'hotdogs) ; Error

(cdr '())

(car (cdr '((b) (x y) ((c))))) ; (x y)

(cdr (cdr '((b) (x y) ((c))))) ; (((c)))

#_(cdr (car '(a (b (c)) d))) ; Error

; A list

; A list

(cons 'peanut '(butter and jelly)) ; (peanut butter and jelly)

(cons '(banana and) '(peanut butter and jelly)) ; ((banana and) (peanut butter and jelly))

(cons '((help) this) '(is very ((hard) to learn))) ; (((help) this) (is very ((hard) to learn)))

; (cons <s-expr> <list>)

(cons '(a b (c)) '()) ; ((a b (c)))

(cons 'a '()) ; (a)

#_(cons '((a b c)) 'b) ; Error

#_(cons 'a 'b) ; Error

(cons 'a (car '((b) c d))) ; (a b)

(cons 'a (cdr '((b) c d))) ; (a c d)

(defn null?
  [x]
  (or (nil? x) (empty? x)))

(nil? '()) ; False
(null? '()) ; True

(nil? '()) ; False
(null? (quote ())) ; True

(nil? '(a b c)) ; False
(null? '(a b c)) ; False

(nil? 'spaghetti) ; False
#_(null? 'spaghetti) ; Error

(atom? 'Harry) ; True

(atom? '(Harry had a heap of apples)) ; False

; (atom? <s-expr>)

(atom? (car '(Harry had a heap of apples))) ; True

(atom? (cdr '(Harry had a heap of apples))) ; False

(atom? (cdr '(Harry))) ; True, differs from book

(atom? (car (cdr '(swing low sweet cherry out)))) ; True

(atom? (car (cdr '(swing (low sweet) cherry out)))) ; False

(= 'Harry 'Harry) ; True

(= 'margarine 'butter) ; False

(= '() '(strawberry)) ; False, differs from book

(= 6 7) ; False, differs from book

(= (car '(Mary has a little lamp chop)) 'Mary) ; True

(= (cdr '(soured milk)) 'milk) ; False, differs from book

(= (car '(beans beans we need jelly beans)) (car (cdr '(beans beans we need jelly beans)))) ; True
