(ns little-clojurer.cons-the-magnificent
  (:use [little-clojurer.toys]
        [little-clojurer.preface]))

(defn rember
  "Remove a member"
  [x xs]
  (cond 
    (null? xs) xs
    (= x (car xs)) (cdr xs)
    :else (cons (car xs) (rember x (cdr xs)))))


(rember 'mint '(lamb chops and mint jelly))
;; (lamb chops and jelly)

(rember 'mint '(lamb chops and mint flavored mint jelly))
;; (lamb chops and flavored mint jelly) 

(rember 'toast '(bacon lettuce and tomato))
;; (bacon lettuce and tomato)

(rember 'cup '(coffee cup tea cup and hick cup))
;; (coffee tea cup and hick cup)

(rember 'bacon '(bacon lettuce and tomato))
;; (lettuce and tomato)

(rember 'and '(bacon lettuce and tomato))
;; (bacon lettuce tomato)

(rember 'sauce '(soy sauce and tomato sauce))
;; (soy and tomato sauce)

(defn firsts
  [l]
  (cond
    (null? l) l
    :else (cons (car (car l)) (firsts (cdr l)))))

(firsts '((apple peach pumpkin)
          (plum pear cherry)
          (grape raisin pea)
          (bean carrot eggplant)))
;; (apple plum grape bean)

(firsts '((a b) (c d) (e f)))
;; (a c e)

(firsts '())
;; ()

(firsts '((five plums)
          (four)
          (eleven green oranges)))
;; (five four eleven)

(firsts '(((five plums) four)
          (eleven green oranges)
          ((no) more)))
;; ((five plums) eleven (no))

(defn seconds
  [l]
  (cond (null? l) l
        :else (cons (car (cdr (car l))) (seconds (cdr l)))))

(seconds '((a b) (c d) (e f)))
;; (b d f)

(defn insertR
  "Insert 'new' to right of 'old' in 'lat'"
  [new old lat]
  (cond
    (null? lat) lat
    (= (car lat) old) (cons old (cons new (cdr lat)))
    :else (cons (car lat) (insertR new old (cdr lat)))))

(insertR 'topping 'fudge '(ice cream with fudge for dessert))
;; (ice cream with fudge topping for dessert)

(insertR 'jalapeno 'and '(tacos tamales and salsa))
;; (tacos tamales and jalapeno salsa)

(insertR 'e 'd '(a b c d f g d h))
;; (a b c d e f g d h)

(defn insertL
  "Insert 'new' to left of 'old' in 'lat'"
  [new old lat]
  (cond
    (null? lat) lat
    (= (car lat) old) (cons new lat)
    :else (cons (car lat) (insertL new old (cdr lat)))))

(insertL 'topping 'fudge '(ice create with fudge for dessert))
;; (ice create with topping fudge for dessert)

(defn subst
  "Replaces the first occurence of 'old' in the 'lat' with 'new'"
  [new old lat]
  (cond
    (null? lat) lat
    (= (car lat) old) (cons new (cdr lat))
    :else (cons (car lat) (subst new old (cdr lat)))))

(subst 'topping 'fudge '(ice create with fudge for dessert))
;; (ice create with topping for dessert)

(defn subst2
  "Replaces either the first occurrence of 'o1' or the first occurrence of 'o2' by 'new'"
  [new o1 o2 lat]
  (cond
    (null? lat) lat
    (or (= (car lat) o1)
        (= (car lat) o2)) (cons new (cdr lat))
    :else (cons (car lat) (subst2 new o1 o2 (cdr lat)))))

(subst2 'vanilla 'chocolate 'banana '(banana ice cream with chocolate topping))
;; (vanilla ice cream with chocolate topping)

(defn multirember
  "Remove all occurrences of 'a' from 'lat'"
  [a lat]
  (cond
    (null? lat) lat
    (= (car lat) a) (multirember a (cdr lat))
    :else (cons (car lat) (multirember a (cdr lat)))))

(multirember 'cup '(coffee cup tea cup and hick cup))
;; (coffee tea and hick)

(defn multiinsertR
  [new old lat]
  (cond
    (null? lat) lat
    (= old (car lat)) (cons old (cons new (multiinsertR new old (cdr lat))))
    :else (cons (car lat) (multiinsertR new old (cdr lat)))))

(multiinsertR 'right 'left '(left x left))
;; (left right x left right)

(defn multiinsertL
  [new old lat]
  (cond
    (null? lat) lat
    (= old (car lat)) (cons new (cons old (multiinsertL new old (cdr lat))))
    :else (cons (car lat) (multiinsertL new old (cdr lat)))))

(multiinsertL 'really-left 'left '(left x left))
;; (really-left left x really-left left)

(defn multisubst
  [new old lat]
  (cond
    (null? lat) lat
    (= (car lat) old) (cons new (multisubst new old (cdr lat)))
    :else (cons (car lat) (multisubst new old (cdr lat)))))

(multisubst 'new 'old '(old not-old old old not-old))
;; (new not-old new new not-old)

