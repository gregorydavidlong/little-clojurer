(ns little-clojurer.friends-and-relations
  (:use [little-clojurer.cons-the-magnificent]
        [little-clojurer.do-it-again]
        [little-clojurer.oh-my-gawd]
        [little-clojurer.preface]
        [little-clojurer.shadows]
        [little-clojurer.toys]))

(defn _set?
  [x]
  (cond
   (empty? x) true
   (member? (first x) (next x)) false
   :default (recur (next x))))

(_set? '(apple peaches apple plum))
;; false

(_set? '(apples peaches pears plums))
;; true

(_set? '(apple 3 pear 4 9 apple 3 4))
;; false

#_(defn makeset
  "Remove all duplicates from the provided list"
  [lat]
  (cond
   (_set? lat) lat
   (member? (first lat) (next lat)) (recur (next lat))
   :default (cons (first lat) (makeset (next lat)))))


(defn makeset
  [lat]
  (cond
   (_set? lat) lat
   :default (cons (first lat) (multirember (first lat) (makeset (next lat))))))

(makeset '(apple peach pear peach plum apple lemon peach))
;; (pear plum apple lemon peach)

(makeset '(apple peach pear peach plum apple lemon peach))
;; (apple peach pear plum lemon)

(makeset '(apple 3 pear 4 9 apple 3 4))
;; (apple 3 pear 4 9)

#_(defn subset?
  [set1 set2]
  (cond
   (empty? set1) true
   (member? (first set1) set2) (recur (next set1) set2)
   :default false))

(defn subset?
  [set1 set2]
  (if (empty? set1)
    true
    (and (member? (first set1) set2)
         (recur (next set1) set2))))

(subset? '(5 chicken wings)
         '(5 hamburgers 2 pieces fried chicken and light duckling wings))
;; true

(subset? '(4 pounds of horseradish)
         '(four pounds chicken and 5 ounces horseradish))
;; false

(defn eqset?
  [set1 set2]
  (and (subset? set1 set2)
       (subset? set2 set1)))

(eqset? '(6 large chickens with wings)
        '(6 chickens with large wings))
;; true

#_(defn intersect?
  [set1 set2]
  (cond
   (empty? set1) false
   (member? (first set1) set2) true
   :default (recur (next set1) set2)))

(defn intersect?
  [set1 set2]
  (if (empty? set1)
    false
    (or (member? (first set1) set2)
        (recur (next set1) set2))))

(intersect? '(stewed tomatoes and macaroni)
            '(macaroni and cheese))
;; true

(defn intersect
  [set1 set2]
  (cond
   (empty? set1) '()
   (member? (first set1) set2) (cons (first set1)
                                     (intersect (next set1) set2))
   :default (recur (next set1) set2)))

(intersect '(stewed tomatoes and macaroni)
           '(macaroni and cheese))
;; (and macaroni)

(defn union
  [set1 set2]
  (cond
   (empty? set1) set2
   (member? (first set1) set2) (recur (next set1) set2)
   :default (cons (first set1)
                  (union (next set1) set2))))

(union '(stewed tomatoes and macaroni casserole)
       '(macaroni and cheese))
;; (stewed tomatoes casserole macaroni and cheese)

;; TODO: implement this
(defn intersectall
  [l-set]
  (cond
   (empty? (next l-set)) (first l-set)
   :default (intersect (first l-set)
                       (intersectall (next l-set)))))

(intersectall '((6 pears and)
                (3 peaches and 6 peppers)
                (8 pears and 6 plums)
                (and 6 prunes with some apples)))
;; (6 and)

(defn a-pair?
  [l]
  (and (list? l)
       (= 2 (count l))))

(a-pair? '(full (house)))
;; true

(a-pair? 3)
;; false

(defn build
  "build a pair"
  [s1 s2]
  (cons s1 (cons s2 '())))

(build 1 2)
;; (1 2)

(first (build 1 2))
;; 1

(second (build 1 2))
;; 2

(defn third
  [p]
  (first (next (next p))))

(third '(1 2 3))
;; 3

(defn rel?
  "A relation, a list of paris"
  [l]
  (and
   (a-pair? (first l))
   (_set? l)
   (if (empty? (next l))
     true
     (rel? (next l)))))

(rel? '(apples peaches pumpkin pie))
;; false

(rel? '((apples peaches)
        (pumpkin pie)
        (apples peaches)))
;; false

(rel? '((apples peaches) (pumpkin pie)))
;; true

(rel? '((4 3) (4 2) (7 6) (6 2) (3 4)))
;; true

(defn fun?
  [l]
  (_set? (firsts l)))

(fun? '((4 3) (4 2) (7 6) (6 2) (3 4)))
;; false

(fun? '((8 3) (4 2) (7 6) (6 2) (3 4)))
;; true

(fun? '((d 4) (b 0) (b 9) (e 5) (g 4)))
;; false

#_(defn revrel
  [l]
  (cond
   (empty? l) '()
   :else (cons (build (second (first l)) (first (first l)))
               (revrel (next l)))))

#_(revrel '((8 a) (pumpkin pie) (got sick)))
;; ((a 8) (pie pumpkin) (sick got))

(defn revpair
  [p]
  (build (second p) (first p)))

(defn revrel
  [l]
  (cond
   (empty? l) '()
   :else (cons (revpair (first l))
               (revrel (next l)))))

(revrel '((8 a) (pumpkin pie) (got sick)))
;; ((a 8) (pie pumpkin) (sick got))

(defn fullfun?
  [l]
  (_set? (seconds l)))

(fullfun? '((8 3) (4 2) (7 6) (6 2) (3 4)))
;; false

(fullfun? '((8 3) (4 8) (7 6) (6 2) (3 4)))
;; true

(fullfun? '((grape raisin)
            (plum prune)
            (stewed prune)))
;; false

(fullfun? '((grape raisin)
            (plum prune)
            (stewed grape)))
;; true

(def one-to-one? fullfun?)

(one-to-one? '((chocolate chip) (doughy cookie)))
;; true
