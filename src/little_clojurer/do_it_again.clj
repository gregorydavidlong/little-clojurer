(ns little-clojurer.do-it-again
  (:use [little-clojurer.toys]
        [little-clojurer.preface]))

(defn lat?
  [l]
  (cond
    (null? l) true
    (atom? (car l)) (lat? (cdr l))
    :else false))

(lat? '(Jack Sprat could eat no chicken fat)) ; True

(lat? '((Jack) Sprat could eat no chicken fat)) ; False

(lat? '(Jack (Sprat could) eat no chicken fat)) ; False

(lat? '()) ; True

; lat? is a list of atoms

(lat? '(bacon and eggs)) ; True

(lat? '(bacon (and eggs))) ; False

(or (null? '()) (atom? '(d e f g))) ; True

(or (null? '(a b c)) (atom? '())) ; False

(defn member?
  [elm ls]
  (cond 
    (null? ls) false
    :else (or (= (car ls) elm)
              (member? elm (cdr ls)))))

(member? 'poached '(fried eggs and scrambled eggs)) ; False

(member? 'meat '(mashed potatoes and meat gravy)) ; True

(member? 'liver '(bagels and lox)) ; False
